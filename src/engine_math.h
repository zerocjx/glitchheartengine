#ifndef MATH_H
#define MATH_H

#include <cmath>

//#define GLM
#ifdef GLM
#include <glm/gtc/matrix_transform.hpp>
namespace math
{
    using v2 = glm::vec2;
    using v3 = glm::vec3;
    using v4 = glm::vec4;
    using m4 = glm::mat4;
    using v2i = glm::i32vec2;
    using v3i = glm::i32vec3;
    using rgb = v3;
    using rgba = v4;
    
    r32 Dot(v2 v1, v2 v2)
    {
        return glm::dot(v1,v2);
    }
    
    r32 Dot(v3 v1, v3 v2)
    {
        return glm::dot(v1,v2);
    }
    
    r32 Dot(v4 v1, v4 v2)
    {
        return glm::dot(v1,v2);
    }
    
    r32 Distance(v2 v1, v2 v2)
    {
        return glm::distance(v1,v2);
    }
    
    m4 translate(m4 M, v3 V)
    {
        return glm::translate(M,V);
    }
    
    m4 Rotate(m4 M, r32 Angle, v3 V)
    {
        return glm::rotate(M, Angle, V);
    }
    
    m4 Inverse(m4 M)
    {
        return glm::inverse(M);
    }
    
    m4 transpose(m4 M)
    {
        return glm::transpose(M);
    }
    
    v3 Project(v3 V, m4 M, m4 P, v4 Vp)
    {
        return glm::project(V,M,P,Vp);
    }
    
    v3 UnProject(v3 V, m4 M, m4 P, v4 Vp)
    {
        return glm::unProject(V,M,P,Vp);
    }
    
    m4 Scale(m4 M, v3 S)
    {
        return glm::scale(M,S);
    }
    
    m4 Ortho(r32 L, r32 R, r32 B, r32 t, r32 N, r32 F)
    {
        return glm::ortho(L,R,B,t,N,F);
    }
    
    v2 Normalize(v2 V)
    {
        return glm::normalize(V);
    }
    
    v3 Normalize(v3 V)
    {
        return glm::normalize(V);
    }
    
    v4 Normalize(v4 V)
    {
        return glm::normalize(V);
    }
    
    inline i32 Round(r32 V)
    {
        return glm::round(V);
    }
    
    r32 Floor(r32 V)
    {
        return glm::floor(V);
    }
    
    v2 Floor(v2 V)
    {
        return glm::floor(V);
    }
    
    v3 Floor(v3 V)
    {
        return glm::floor(V);
    }
    
    r32 Ceil(r32 V)
    {
        return glm::ceil(V);
    }
    
    v2 Ceil(v2 V)
    {
        return glm::ceil(V);
    }
    
    r32 Length(v2 V)
    {
        return glm::length(V);
    }
    
    r32 Length(v3 V)
    {
        return glm::length(V);
    }
    
    r32 Length(v4 V)
    {
        return glm::length(V);
    }
}
#else
namespace math
{
    
    inline i32 clamp(i32 Minimum, i32 Value, i32 Maximum)
    {
        i32 result = MAX(Minimum, MIN(Value,Maximum));
        return result;
    }
    
    inline r32 clamp(r32 Minimum, r32 Value, r32 Maximum)
    {
        return MAX(Minimum, MIN(Value,Maximum));
    }
    
    inline r64 clamp(r64 Minimum, r64 Value, r64 Maximum)
    {
        return MAX(Minimum, MIN(Value,Maximum));
    }
    
    union Vec2
    {
        struct
        {
            r32 x;
            r32 y;
        };
        struct
        {
            r32 u;
            r32 v;
        };
        r32 e[2];
        Vec2(r32 x, r32 y) : x(x), y(y){}
        Vec2() : x(0.0f), y(0.0f) {}
        Vec2(r32 i) : e{i,i} {}
        Vec2(r32 i[2]) : e{i[0],i[1]} {}
        Vec2(const Vec2& o) = default;
        Vec2(i32 x, i32 y) : x((r32)x), y((r32)y) {}
        Vec2(r64 x, r64 y) : x((r32)x), y((r32)y) {}
        Vec2(r32 x, r64 y) : x(x), y((r32)y) {}
        Vec2(r32 x, i32 y) : x(x), y((r32)y) {}
        Vec2(i32 x, r32 y) : x((r32)x), y(y) {}
        Vec2(i32 x, r64 y) : x((r32)x), y((r32)y) {}
        Vec2(r64 x, i32 y) : x((r32)x), y((r32)y) {}
        Vec2(r64 x, r32 y) : x((r32)x), y(y) {}
        
        Vec2& operator=(const Vec2& v) = default;
        
        r32 operator[](i32 i)
        {
            return this->e[i];
        }
        
        inline Vec2 operator* (Vec2 o)
        {
            Vec2 result(*this);
            result.x *= o.x;
            result.y *= o.y;
            return result;
        }
        
        inline Vec2 operator+ (Vec2 o)
        {
            Vec2 result(*this);
            result.x += o.x;
            result.y += o.y;
            return result;
        }
        
        inline void operator*= (Vec2 o)
        {
            this->x *= o.x;
            this->y *= o.y;
        }
        
        inline void operator+= (Vec2 o)
        {
            this->x += o.x;
            this->y += o.y;
        }
        
        inline Vec2 operator+ (r32 s)
        {
            Vec2 result(*this);
            result.x += s;
            result.y += s;
            return result;
        }
        
        inline Vec2 operator* (r32 s)
        {
            Vec2 result(*this);
            result.x *= s;
            result.y *= s;
            return result;
        }
        
        inline Vec2 operator/ (r32 s)
        {
            Vec2 result(*this);
            result.x /= s;
            result.y /= s;
            return result;
        }
        
        inline void operator+= (r32 s)
        {
            this->x += s;
            this->y += s;
        }
        
        inline void operator*= (r32 s)
        {
            this->x *= s;
            this->y *= s;
        }
        
        inline void operator/= (r32 s)
        {
            this->x /= s;
            this->y /= s;
        }
        
        inline void operator-= (r32 s)
        {
            this->x -= s;
            this->y -= s;
        }
        
        inline Vec2 operator- (Vec2 o)
        {
            Vec2 result(*this);
            result.x -= o.x;
            result.y -= o.y;
            return result;
        }
        
        inline void operator-= (Vec2 o)
        {
            this->x -= o.x;
            this->y -= o.y;
        }
        
        inline Vec2 operator- (r32 s)
        {
            Vec2 result(*this);
            result.x -= s;
            result.y -= s;
            return result;
        }
        
        inline Vec2 operator/ (Vec2 o)
        {
            Vec2 result(*this);
            result.x /= o.x;
            result.y /= o.y;
            return result;
        }
        
        inline void operator/= (Vec2 o)
        {
            this->x /= o.x;
            this->y /= o.y;
        }
    };
    
    union Vec3
    {
        struct 
        {
            union
            {
                Vec2 xy;
                struct
                {
                    r32 x, y;
                };
            };
            r32 z;
            
            
        };
        struct 
        {
            union
            {
                Vec2 rg;
                struct
                {
                    r32 r, g;
                };
            };
            
            r32 b;
        };
        
        r32 e[3];
        Vec3(r32 x, r32 y, r32 z) : x(x), y(y), z(z) {}
        Vec3() : x(0.0f), y(0.0f), z(0.0f) {}
        Vec3(r32 i) : e{i,i,i} {}
        Vec3(r32 i[3]) : e{i[0],i[1], i[2]} {}
        Vec3(const Vec3& o) : e{o.x, o.y, o.z} {}
        Vec3(r64 x, r64 y, r64 z) : x((r32)x), y((r32)y), z((r32)z) {}
        Vec3(r64 x, i32 y, r64 z) : x((r32)x), y((r32)y), z((r32)z) {}
        Vec3(i32 x, i32 y, i32 z) : x((r32)x), y((r32)y), z((r32)z) {}
        Vec3(i32 x, r32 y, i32 z) : x((r32)x), y(y), z((r32)z) {}
        Vec3(r64 x, r64 y, i32 z) : x((r32)x), y((r32)y), z((r32)z) {}
        Vec3(r32 x, r32 y, i32 z) : x(x), y(y), z((r32)z) {}
        Vec3(r32 x, i32 y, i32 z) : x(x), y((r32)y), z((r32)z) {}
        Vec3(i32 x, i32 y, r32 z) : x((r32)x), y((r32)y), z(z) {}
        Vec3(r32 x, r32 y, r64 z) : x(x), y(y), z((r32)z) {}
        Vec3(r32 x, i32 y, r32 z) : x(x), y((r32)y), z(z) {}
        Vec3(r64 x, r32 y, r64 z) : x((r32)x), y(y), z((r32)z) {}
        Vec3(r64 x, r32 y, r32 z) : x((r32)x), y(y), z(z) {}
        Vec3(Vec2 v, r32 z) : x(v.x), y(v.y), z(z) {}
        
        Vec3& operator=(const Vec3& v) = default;
        
        r32& operator[](i32 i)
        {
            return this->e[i];
        }
        
        inline Vec3 operator= (Vec2 o)
        {
            return Vec3(o.x, o.y, 0);
        }
        
        inline Vec3 operator-()
        {
            Vec3 result(1.0f);
            result.x = -this->x;
            result.y = -this->y;
            result.z = -this->z;
            return result;
        }
        
        inline Vec3 operator* (Vec3 o)
        {
            Vec3 result(*this);
            result.x *= o.x;
            result.y *= o.y;
            result.z *= o.z;
            return result;
        }
        
        inline Vec3 operator+ (Vec3 o)
        {
            Vec3 result(*this);
            result.x += o.x;
            result.y += o.y;
            result.z += o.z;
            return result;
        }
        
        inline void operator*= (Vec3 o)
        {
            this->x *= o.x;
            this->y *= o.y;
            this->z *= o.z;
        }
        
        inline void operator+= (Vec3 o)
        {
            this->x += o.x;
            this->y += o.y;
            this->z += o.z;
        }
        
        inline Vec3 operator+ (r32 s)
        {
            Vec3 result(*this);
            result.x += s;
            result.y += s;
            result.z += s;
            return result;
        }
        
        inline Vec3 operator* (r32 s)
        {
            Vec3 result(*this);
            result.x *= s;
            result.y *= s;
            result.z *= s;
            return result;
        }
        
        inline Vec3 operator/ (r32 s)
        {
            Vec3 result(*this);
            result.x /= s;
            result.y /= s;
            result.z /= s;
            return result;
        }
        
        inline void operator+= (r32 s)
        {
            this->x += s;
            this->y += s;
            this->z += s;
        }
        
        inline void operator*= (r32 s)
        {
            this->x *= s;
            this->y *= s;
            this->z *= s;
        }
        
        inline void operator/= (r32 s)
        {
            this->x /= s;
            this->y /= s;
            this->z /= s;
        }
        
        inline void operator-= (r32 s)
        {
            this->x -= s;
            this->y -= s;
            this->z -= s;
        }
        
        inline Vec3 operator- (Vec3 o)
        {
            Vec3 result(*this);
            result.x -= o.x;
            result.y -= o.y;
            result.z -= o.z;
            return result;
        }
        
        inline void operator-= (Vec3 o)
        {
            this->x -= o.x;
            this->y -= o.y;
            this->z -= o.z;
        }
        
        inline Vec3 operator- (r32 s)
        {
            Vec3 result(*this);
            result.x -= s;
            result.y -= s;
            result.z -= s;
            return result;
        }
        
        inline Vec3 operator/ (Vec3 o)
        {
            Vec3 result(*this);
            result.x /= o.x;
            result.y /= o.y;
            result.z /= o.z;
            return result;
        }
        
        inline void operator/= (Vec3 o)
        {
            this->x /= o.x;
            this->y /= o.y;
            this->z /= o.z;
        }
    };
    
    union Vec4
    {
        struct 
        {
            union
            {
                Vec3 xyz;
                struct
                {
                    r32 x, y, z;
                };
                struct
                {
                    
                    Vec2 xy;
                };
            };
            r32 w;
            
        };
        struct 
        {
            union
            {
                Vec3 rgb;
                struct
                {
                    r32 r, g, b;
                };
            };
            r32 a;
        };
        r32 e[4];
        
        Vec4(r32 x, r32 y, r32 z, r32 w) : x(x), y(y), z(z), w(w) {}
        Vec4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
        Vec4(r32 i) : e{i,i,i,i} {}
        Vec4(r32 i[4]) : e{i[0], i[1], i[2], i[3]} {}
        Vec4(const Vec4& o) : x(o.x), y(o.y), z(o.z), w(o.w) {}
        
        Vec4(i32 x, i32 y, i32 z, i32 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(r32 x, r32 y, r32 z, i32 w) : 
        x(x), y(y), z(z), w((r32)w) {}
        
        Vec4(r64 x, r64 y, r64 z, r64 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(r64 x, r64 y, r64 z, i32 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(r32 x, i32 y, r32 z, i32 w) : 
        x(x), y((r32)y), z(z), w((r32)w) {}
        
        Vec4(i32 x, r64 y, i32 z, i32 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(r64 x, i32 y, i32 z, i32 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(i32 x, i32 y, i32 z, r64 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(r32 x, i32 y, i32 z, i32 w) : 
        x(x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(i32 x, i32 y, i32 z, r32 w) : 
        x((r32)x), y((r32)y), z((r32)z), w(w) {}
        
        Vec4(r64 x, r64 y, i32 z, r64 w) : 
        x((r32)x), y((r32)y), z((r32)z), w((r32)w) {}
        
        Vec4(Vec3 o, r32 w) : x(o.x), y(o.y), z(o.z), w(w) {}
        
        Vec4(Vec2 v, r32 z, r32 w) : x(v.x), y(v.y), z(z), w(w) {} 
        
        Vec4& operator=(const Vec4& v) = default;
        
        r32 operator[](i32 i)
        {
            return this->e[i];
        }
        
        inline Vec4 operator* (Vec4 o)
        {
            Vec4 result(*this);
            result.x *= o.x;
            result.y *= o.y;
            result.z *= o.z;
            result.w *= o.w;
            return result;
        }
        
        inline Vec4 operator+ (Vec4 o)
        {
            Vec4 result(*this);
            result.x += o.x;
            result.y += o.y;
            result.z += o.z;
            result.w += o.w;
            return result;
        }
        
        inline void operator*= (Vec4 o)
        {
            this->x *= o.x;
            this->y *= o.y;
            this->z *= o.z;
            this->w *= o.w;
        }
        
        inline void operator+= (Vec4 o)
        {
            this->x += o.x;
            this->y += o.y;
            this->z += o.z;
            this->w += o.w;
        }
        
        inline Vec4 operator+ (r32 s)
        {
            Vec4 result(*this);
            result.x += s;
            result.y += s;
            result.z += s;
            result.w += s;
            return result;
        }
        
        inline Vec4 operator* (r32 s)
        {
            Vec4 result(*this);
            result.x *= s;
            result.y *= s;
            result.z *= s;
            result.w *= s;
            return result;
        }
        
        inline Vec4 operator/ (r32 s)
        {
            Vec4 result(*this);
            result.x /= s;
            result.y /= s;
            result.z /= s;
            result.w /= s;
            return result;
        }
        
        inline void operator+= (r32 s)
        {
            this->x += s;
            this->y += s;
            this->z += s;
            this->w += s;
        }
        
        inline void operator*= (r32 s)
        {
            this->x *= s;
            this->y *= s;
            this->z *= s;
            this->w *= s;
        }
        
        inline void operator/= (r32 s)
        {
            this->x /= s;
            this->y /= s;
            this->z /= s;
            this->w /= s;
        }
        
        inline void operator-= (r32 s)
        {
            this->x -= s;
            this->y -= s;
            this->z -= s;
            this->w -= s;
        }
        
        inline Vec4 operator- (Vec4 o)
        {
            Vec4 result(*this);
            result.x -= o.x;
            result.y -= o.y;
            result.z -= o.z;
            result.w -= o.w;
            return result;
        }
        
        inline void operator-= (Vec4 o)
        {
            this->x -= o.x;
            this->y -= o.y;
            this->z -= o.z;
            this->w -= o.w;
        }
        
        inline Vec4 operator- (r32 s)
        {
            Vec4 result(*this);
            result.x -= s;
            result.y -= s;
            result.z -= s;
            result.w -= s;
            return result;
        }
        
        inline Vec4 operator/ (Vec4 o)
        {
            Vec4 result(*this);
            result.x /= o.x;
            result.y /= o.y;
            result.z /= o.z;
            result.w /= o.w;
            return result;
        }
        
        inline void operator/= (Vec4 o)
        {
            this->x /= o.x;
            this->y /= o.y;
            this->z /= o.z;
            this->w /= o.w;
        }
    };
    
    union Vec2i
    {
        struct
        {
            i32 x,y;
        };
        i32 e[2];
        Vec2i(i32 x, i32 y) : x(x), y(y){}
        Vec2i(r32 x, r32 y) : x((i32)x), y((i32)y){}
        Vec2i() : x(0),y(0) {}
        Vec2i(i32 i) : x(i), y(i) {}
        Vec2i(i32 i[2]) : e{i[0],i[1]} {}
        
        inline Vec2i operator* (Vec2i o)
        {
            Vec2i result(*this);
            result.x *= o.x;
            result.y *= o.y;
            return result;
        }
        
        inline Vec2i operator+ (Vec2i o)
        {
            Vec2i result(*this);
            result.x += o.x;
            result.y += o.y;
            return result;
        }
        
        inline void operator*= (Vec2i o)
        {
            this->x *= o.x;
            this->y *= o.y;
        }
        
        inline void operator+= (Vec2i o)
        {
            this->x += o.x;
            this->y += o.y;
        }
        
        inline Vec2i operator+ (i32 s)
        {
            Vec2i result(*this);
            result.x += s;
            result.y += s;
            return result;
        }
        
        inline Vec2i operator* (i32 s)
        {
            Vec2i result(*this);
            result.x *= s;
            result.y *= s;
            return result;
        }
        
        inline Vec2i operator/ (i32 s)
        {
            Vec2i result(*this);
            result.x /= s;
            result.y /= s;
            return result;
        }
        
        inline void operator+= (i32 s)
        {
            this->x += s;
            this->y += s;
        }
        
        inline void operator*= (i32 s)
        {
            this->x *= s;
            this->y *= s;
        }
        
        inline void operator/= (i32 s)
        {
            this->x /= s;
            this->y /= s;
        }
        
        inline void operator-= (i32 s)
        {
            this->x -= s;
            this->y -= s;
        }
        
        inline Vec2i operator- (Vec2i o)
        {
            Vec2i result(*this);
            result.x -= o.x;
            result.y -= o.y;
            return result;
        }
        
        inline void operator-= (Vec2i o)
        {
            this->x -= o.x;
            this->y -= o.y;
        }
        
        inline Vec2i operator- (i32 s)
        {
            Vec2i result(*this);
            result.x -= s;
            result.y -= s;
            return result;
        }
        
        inline Vec2i operator/ (Vec2i o)
        {
            Vec2i result(*this);
            result.x /= o.x;
            result.y /= o.y;
            return result;
        }
        
        inline void operator/= (Vec2i o)
        {
            this->x /= o.x;
            this->y /= o.y;
        }
    };
    
    union Vec3i
    {
        struct
        {
            i32 x,y,z;
        };
        i32 e[3];
        Vec3i(i32 x, i32 y, i32 z) : x(x), y(y), z(z){}
        Vec3i() : x(0), y(0), z(0) {}
        Vec3i(i32 i) : x(i), y(i), z(i) {}
        Vec3i(i32 i[3]) : e{i[0], i[1], i[2]} {}
        Vec3i(Vec3 v) : e{(i32)v.e[0], (i32)v.e[1], (i32)v.e[2]} {}
        
        inline Vec3i operator* (Vec3i o)
        {
            Vec3i result(*this);
            result.x *= o.x;
            result.y *= o.y;
            result.z *= o.z;
            return result;
        }
        
        inline Vec3i operator+ (Vec3i o)
        {
            Vec3i result(*this);
            result.x += o.x;
            result.y += o.y;
            result.z += o.z;
            return result;
        }
        
        inline void operator*= (Vec3i o)
        {
            this->x *= o.x;
            this->y *= o.y;
            this->z *= o.z;
        }
        
        inline void operator+= (Vec3i o)
        {
            this->x += o.x;
            this->y += o.y;
            this->z += o.z;
        }
        
        inline Vec3i operator+ (i32 s)
        {
            Vec3i result(*this);
            result.x += s;
            result.y += s;
            result.z += s;
            return result;
        }
        
        inline Vec3i operator* (i32 s)
        {
            Vec3i result(*this);
            result.x *= s;
            result.y *= s;
            result.z *= s;
            return result;
        }
        
        inline Vec3i operator/ (i32 s)
        {
            Vec3i result(*this);
            result.x /= s;
            result.y /= s;
            result.z /= s;
            return result;
        }
        
        inline void operator+= (i32 s)
        {
            this->x += s;
            this->y += s;
            this->z += s;
        }
        
        inline void operator*= (i32 s)
        {
            this->x *= s;
            this->y *= s;
            this->z *= s;
        }
        
        inline void operator/= (i32 s)
        {
            this->x /= s;
            this->y /= s;
            this->z /= s;
        }
        
        inline void operator-= (i32 s)
        {
            this->x -= s;
            this->y -= s;
            this->z -= s;
        }
        
        inline Vec3i operator- (Vec3i o)
        {
            Vec3i result(*this);
            result.x -= o.x;
            result.y -= o.y;
            result.z -= o.z;
            return result;
        }
        
        inline void operator-= (Vec3i o)
        {
            this->x -= o.x;
            this->y -= o.y;
            this->z -= o.z;
        }
        
        inline Vec3i operator- (i32 s)
        {
            Vec3i result(*this);
            result.x -= s;
            result.y -= s;
            result.z -= s;
            return result;
        }
        
        inline Vec3i operator/ (Vec3i o)
        {
            Vec3i result(*this);
            result.x /= o.x;
            result.y /= o.y;
            result.z /= o.z;
            return result;
        }
        
        inline void operator/= (Vec3i o)
        {
            this->x /= o.x;
            this->y /= o.y;
            this->z /= o.z;
        }
        
    };
    
    
    union Vec4i
    {
        struct
        {
            i32 x, y, z, w;
        };
        i32 e[4];
        Vec4i(i32 x, i32 y, i32 z, i32 w) : x(x), y(y), z(z), w(w){}
        Vec4i() : x(0), y(0), z(0), w(0) {}
        Vec4i(i32 i) : x(i), y(i), z(i), w(i) {}
        Vec4i(i32 i[4]) : e{i[0], i[1], i[2], i[3]} {}
        
        inline Vec4i operator* (Vec4i o)
        {
            Vec4i result(*this);
            result.x *= o.x;
            result.y *= o.y;
            result.z *= o.z;
            result.w *= o.w;
            return result;
        }
        
        inline Vec4i operator+ (Vec4i o)
        {
            Vec4i result(*this);
            result.x += o.x;
            result.y += o.y;
            result.z += o.z;
            result.w += o.w;
            return result;
        }
        
        inline void operator*= (Vec4i o)
        {
            this->x *= o.x;
            this->y *= o.y;
            this->z *= o.z;
            this->w *= o.w;
        }
        
        inline void operator+= (Vec4i o)
        {
            this->x += o.x;
            this->y += o.y;
            this->z += o.z;
            this->w += o.w;
        }
        
        inline Vec4i operator+ (i32 s)
        {
            Vec4i result(*this);
            result.x += s;
            result.y += s;
            result.z += s;
            result.w += s;
            return result;
        }
        
        inline Vec4i operator* (i32 s)
        {
            Vec4i result(*this);
            result.x *= s;
            result.y *= s;
            result.z *= s;
            result.w *= s;
            return result;
        }
        
        inline Vec4i operator/ (i32 s)
        {
            Vec4i result(*this);
            result.x /= s;
            result.y /= s;
            result.z /= s;
            result.w /= s;
            return result;
        }
        
        inline void operator+= (i32 s)
        {
            this->x += s;
            this->y += s;
            this->z += s;
            this->w += s;
        }
        
        inline void operator*= (i32 s)
        {
            this->x *= s;
            this->y *= s;
            this->z *= s;
            this->w *= s;
        }
        
        inline void operator/= (i32 s)
        {
            this->x /= s;
            this->y /= s;
            this->z /= s;
            this->w /= s;
        }
        
        inline void operator-= (i32 s)
        {
            this->x -= s;
            this->y -= s;
            this->z -= s;
            this->w -= s;
        }
        
        inline Vec4i operator- (Vec4i o)
        {
            Vec4i result(*this);
            result.x -= o.x;
            result.y -= o.y;
            result.z -= o.z;
            result.w -= o.w;
            return result;
        }
        
        inline void operator-= (Vec4i o)
        {
            this->x -= o.x;
            this->y -= o.y;
            this->z -= o.z;
            this->w -= o.w;
        }
        
        inline Vec4i operator- (i32 s)
        {
            Vec4i result(*this);
            result.x -= s;
            result.y -= s;
            result.z -= s;
            result.w -= s;
            return result;
        }
        
        inline Vec4i operator/ (Vec4i o)
        {
            Vec4i result(*this);
            result.x /= o.x;
            result.y /= o.y;
            result.z /= o.z;
            result.w /= o.w;
            return result;
        }
        
        inline void operator/= (Vec4i o)
        {
            this->x /= o.x;
            this->y /= o.y;
            this->z /= o.z;
            this->w /= o.w;
        }
        
    };
    
    
    union Mat4
    {
        struct
        {
            r32 m11,m12,m13,m14;
            r32 m21,m22,m23,m24;
            r32 m31,m32,m33,m34;
            r32 m41,m42,m43,m44;
        };
        struct
        {
            r32 a, b, c, d;
            r32 e, f, g, h;
            r32 i, j, k, l;
            r32 m, n, o, p;
        };
        struct
        {
            r32 m0[4];
            r32 m1[4];
            r32 m2[4];
            r32 m3[4];
        };
        struct
        {
            Vec4 v1;
            Vec4 v2;
            Vec4 v3;
            Vec4 v4;
        };
        r32 v[4][4];
        r32 q[16];
        
        inline r32* operator[](i32 idx)
        {
            return this->v[idx];
        }
        
        Mat4() : v{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}} {}
        Mat4(r32 m11, r32 m12, r32 m13, r32 m14, 
             r32 m21, r32 m22, r32 m23, r32 m24,
             r32 m31, r32 m32, r32 m33, r32 m34,
             r32 m41, r32 m42, r32 m43, r32 m44) : 
        m11(m11), m12(m12), m13(m13), m14(m14),
        m21(m21), m22(m22), m23(m23), m24(m24),
        m31(m31), m32(m32), m33(m33), m34(m34),
        m41(m41), m42(m42), m43(m43), m44(m44) {}
        
        Mat4(r32 m0[4], r32 m1[4], r32 m2[4], r32 m3[4]) : 
        m0 {m0[0],m0[1],m0[2],m0[3]}, 
        m1 {m1[0],m1[1],m1[2],m1[3]}, 
        m2 {m2[0],m2[1],m2[2],m2[3]}, 
        m3 {m3[0],m3[1],m3[2],m3[3]} {}
        
        Mat4(r32 i[4][4]) : 
        v{ {i[0][0],i[0][1],i[0][2],i[0][3]}, 
            {i[1][0],i[1][1],i[1][2],i[1][3]}, 
            {i[2][0],i[2][1],i[2][2],i[2][3]}, 
            {i[3][0],i[3][1],i[3][2],i[3][3]}}
        {}
        
        Mat4(r32 i) : v {{i,0,0,0},{0,i,0,0},{0,0,i,0},{0,0,0,i}} {}
        
        Mat4(const Mat4& o) : v{ {o.v[0][0],o.v[0][1],o.v[0][2],o.v[0][3]}, 
            {o.v[1][0],o.v[1][1],o.v[1][2],o.v[1][3]}, 
            {o.v[2][0],o.v[2][1],o.v[2][2],o.v[2][3]}, 
            {o.v[3][0],o.v[3][1],o.v[3][2],o.v[3][3]}} {}
        
        
        Mat4& operator=(const Mat4& m) = default;
        
        Mat4 operator*(Mat4 other)
        {
            Mat4 result(*this);
            for(i32 outer = 0; outer < 4; outer++)
            {
                for(i32 inner = 0; inner < 4; inner++)
                {
                    r32 sum = 0;
                    for(i32 shared = 0; shared < 4; shared++)
                    {
                        sum += this->v[inner][shared] * other.v[shared][outer];
                    }
                    result.v[inner][outer] = sum;
                }
            }
            return result;
        }
        
        // Only __absolute__ convenience: __always__ better to control order yourself
        void operator *= (Mat4 other)
        {
            memcpy(this->v,(other * (*this)).v, sizeof(r32) * 4 * 4);
        }
        
        inline Mat4 operator*(r32 s)
        {
            Mat4 result(*this);
            result.m11 *= s;
            result.m12 *= s;
            result.m13 *= s;
            result.m14 *= s;
            result.m21 *= s;
            result.m22 *= s;
            result.m23 *= s;
            result.m24 *= s;
            result.m31 *= s;
            result.m32 *= s;
            result.m33 *= s;
            result.m34 *= s;
            result.m41 *= s;
            result.m42 *= s;
            result.m43 *= s;
            result.m44 *= s;
            return result;
        }
        
    };
    
    inline Mat4 operator*(r32 s, Mat4 m)
    {
        Mat4 result(m);
        result.m11 *= s;
        result.m12 *= s;
        result.m13 *= s;
        result.m14 *= s;
        result.m21 *= s;
        result.m22 *= s;
        result.m23 *= s;
        result.m24 *= s;
        result.m31 *= s;
        result.m32 *= s;
        result.m33 *= s;
        result.m34 *= s;
        result.m41 *= s;
        result.m42 *= s;
        result.m43 *= s;
        result.m44 *= s;
        return result;
    }
    
    union Quat
    {
        struct
        {
            r32 x, y, z, w;
        };
        struct
        {
            Vec4 axis_angle;
        };
        struct
        {
            Vec3 axis;
            r32 angle;
        };
        
        // Identity quaternion
        Quat() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) {}
        Quat(r32 x, r32 y, r32 z, r32 angle) : axis(Vec3(x * (r32)sin(angle / 2.0f), y * (r32)sin(angle / 2.0f), z * (r32)sin(angle / 2.0f))), angle((r32)cos(angle / 2.0f)) {}
        Quat(Vec3 axis, r32 angle) : 
        axis(Vec3(axis.x * (r32)sin(angle / 2.0f), axis.y * (r32)sin(angle / 2.0f), axis.z * (r32)sin(angle / 2.0f))),
        angle((r32)cos(angle / 2.0f)) {}
        Quat(const Quat& o) : axis(o.axis), angle(o.angle) {}
        
        Quat& operator=(const Quat& q) = default;
        
        inline Quat operator-()
        {
            Quat result(*this);
            result.x = -this->x;
            result.y = -this->y;
            result.z = -this->z;
            result.w = -this->w;
            return result;
        }
        
        inline Quat operator+ (Quat q)
        {
            Quat result(*this);
            result.x += q.x;
            result.y += q.y;
            result.z += q.z;
            result.w += q.w;
            return result;
        }
        
        inline void operator+= (Quat q)
        {
            this->x += q.x;
            this->y += q.y;
            this->z += q.z;
            this->w += q.w;
        }
        
        inline Quat operator- (Quat q)
        {
            Quat result(*this);
            result.x -= q.x;
            result.y -= q.y;
            result.z -= q.z;
            result.w -= q.w;
            return result;
        }
        
        inline Quat operator* (Quat q)
        {
            Quat result(*this);
            result.w = this->w * q.w - (this->x * q.x + this->y * q.y + this->z * q.z);
            result.x = this->w * q.x + this->x * q.w + this->y * q.z - this->z * q.y; 
            result.y = this->w * q.y - this->x * q.z + this->y * q.w + this->z * q.z;
            result.z = this->w * q.z + this->x * q.y - this->y * q.x + this->z * q.w;
            return result;
        }
        
        inline void operator*= (Quat q)
        {
            auto result = *this * q;
            this->x = result.x;
            this->y = result.y;
            this->z = result.z;
            this->w = result.w;
        }
        
        inline Quat operator* (r32 v)
        {
            Quat result(*this);
            result.w *= v;
            result.x *= v;
            result.y *= v;
            result.z *= v;
            return result;
        }
        
        inline Quat operator/ (r32 v)
        {
            Quat result(*this);
            
            result.w /= v;
            result.x /= v;
            result.y /= v;
            result.z /= v;
            
            return result;
        }
    };
    
    b32 is_identity(Quat q);
    r32 dot(Quat q1, Quat q2);
    r32 dot(Vec2 v1, Vec2 v2);
    r32 dot(Vec3 v1, Vec3 v2);
    r32 dot(Vec4 v1, Vec4 v2);
    i32 dot(Vec2i v1, Vec2i v2);
    i32 dot(Vec3i v1, Vec3i v2);
    r32 distance(Vec2 v1, Vec2 v2);
    r32 distance(Vec3 v1, Vec3 v2);
    r32 distance(Vec4 v1, Vec4 v2);
    i32 distance(Vec2i v1, Vec2i v2);
    i32 distance(Vec3i v1, Vec3i v2);
    r32 distance(Vec3i v1, Vec3 v2);
    i32 floor(r32 v);
    Vec2 floor(Vec2 v);
    Vec3 floor(Vec3 v);
    i32 ceil(r32 v);
    Vec2 ceil(Vec2 v);
    Vec3 ceil(Vec3 v);
    r32 sin(r32 v);
    r32 cos(r32 v);
    r32 acos(r32 v);
    r32 absolute(r32 v);
    Vec2 absolute(Vec2 v);
    Vec3 absolute(Vec3 v);
    Vec4 absolute(Vec4 v);
    i32 round(r32 v);
    r32 square(r32 v);
    r32 sqrt(r32 v);
    r32 pow(r32 v, i32 e);
    r32 sin(r32 v);
    r32 cos(r32 v);
    r32 length(Vec2 v);
    r32 length(Vec3 v);
    r32 length(Vec4 v);
    Vec2 normalize(Vec2 v);
    Vec3 normalize(Vec3 v);
    Vec4 normalize(Vec4 v);
    Quat normalize(Quat q);
    Mat4 scale(Mat4 in, Vec3 scale);
    Mat4 translate(Mat4 in, Vec3 translate);
    Mat4 x_rotate(r32 angle);
    Mat4 y_rotate(r32 angle);
    Mat4 z_rotate(r32 angle);
    Mat4 create_rotation(r32 x_angle, r32 y_angle, r32 z_angle);
    
    Quat rotate(Quat in, r32 a, Vec3 axis);
    Mat4 rotate(Mat4 m, r32 a, Vec3 axis);
    Quat conjugate(Quat q);
    r32 magnitude(Quat q);
    r32 get_angle_in_radians(Quat q);
    Vec3 get_axis(Quat q);
    Vec3 right(Mat4 m);
    Vec3 up(Mat4 m);
    Vec3 forward(Mat4 m);
    Vec3 translation(Mat4 m);
    Vec3 scale(Mat4 m);
    Vec3 project(Vec3 in, Mat4 m, Mat4 p, Vec4 viewport);
    Vec3 cross(Vec3 a, Vec3 b);
    Mat4 ortho(r32 left, r32 right, r32 bottom, r32 top, r32 near, r32 far);
    Mat4 look_at(Vec3 p, Vec3 t);
    Mat4 perspective(r32 aspect_width_over_height, r32 focal_length, r32 near, r32 far);
    Mat4 frustum(r32 bottom, r32 top, r32 left, r32 right,
                 r32 near, r32 far);
    Vec3 mult_point_matrix(Vec3 in, Mat4 m);
    Vec3 un_project(Vec3 in, Mat4 model, Mat4 projection, Vec4i viewport);
    
    r32 random_float(r32 from, r32 to);
    Vec3 cast_ray(r32 mouse_x, r32 mouse_y, r32 width, r32 height, Mat4 p, Mat4 v, r32 near);
    
    Quat slerp(Quat q0, Quat q1, r32 t);
    r32 lerp(r32 a, r32 t, r32 b);
    Vec2 lerp(Vec2 a, r32 t, Vec2 b);
    Vec3 lerp(Vec3 a, r32 t, Vec3 b);
    Vec4 lerp(Vec4 a, r32 t, Vec4 b);
    Quat lerp(Quat q0, Quat q1, r32 t);
    Quat nlerp(Quat q0, Quat q1, r32 t);
    Quat interpolate(Quat q0, Quat q1, r32 f);
    Mat4 transpose(Mat4 in);
    Mat4 to_matrix(Quat q);
    Vec4 transform(Mat4& m, const Vec4& v);
    r32 determinant(const Mat4& in);
    Mat4 inverse(Mat4 m);
    
    inline b32 is_identity(Quat q)
    {
        return q.x == 0.0f && q.y == 0.0f && q.z == 0.0f && q.w == 1.0f;
    }
    
    inline Quat operator* (r32 v, Quat q)
    {
        Quat result(q);
        result.w *= v;
        result.x *= v;
        result.y *= v;
        result.z *= v;
        return result;
    }
    
    inline r32 dot(Quat q1, Quat q2)
    {
        r32 result;
        result = q1.w * q2.w + q1.x * q2.x + q1.y * q2.y + q1.z * q2.z;
        return result;
    }
    
    inline Quat conjugate(Quat q)
    {
        Quat result(-q.axis, q.w);
        return result;
    }
    
    inline r32 magnitude(Quat q)
    {
        r32 result = 0.0f;
        result = q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z;
        result = sqrt(result);
        return result;
    }
    
    inline Quat normalize(Quat q)
    {
        return q / magnitude(q);
    }
    
    //@Incomplete JBlow, CaseyM, ShawnM say don't use this
    inline Quat slerp(Quat q0, Quat q1, r32 t)
    {
        q0 = normalize(q0);
        q1 = normalize(q1);
        
        auto dot_p = dot(q0, q1);
        
        const r64 dot_threshold = 0.9995;
        if(dot_p > dot_threshold)
        {
            Quat result = q0 + t * (q1 - q0);
            result = normalize(result);
            return result;
        }
        
        if(dot_p < 0.0f)
        {
            q1 = -q1;
            dot_p = -dot_p;
        }
        
        clamp(dot_p, -1.0f, 1.0f);
        auto theta_0 = acos(dot_p);
        auto theta = theta_0 * t;
        
        auto q2 = q1 - q0 * dot_p;
        q2 = normalize(q2);
        
        auto result = q0 * cos(theta) + q2 * sin(theta);
        result = normalize(result);
        return result;
    }
    
    inline Quat lerp(Quat q0, Quat q1, r32 t)
    {
        return (1.0f - MIN(1.0f,t)) * q0 + t * q1;
    }
    
    inline Quat nlerp(Quat q0, Quat q1, r32 t)
    {
        q0 = normalize(q0);
        q1 = normalize(q1);
        
        auto dot_p = dot(q0, q1);
        
        if(dot_p < 0.0f)
        {
            q1 = -q1;
        }
        
        return normalize(lerp(q0, q1, t));
    }
    
    inline Quat interpolate(Quat q0, Quat q1, r32 f)
    {
        r32 cosom = q0.x * q1.x + q0.y * q1.y + q0.z * q1.z + q0.w * q1.w;
        auto end = q1;
        
        if(cosom < 0.0f)
        {
            cosom = -cosom;
            end.x = -end.x;   // Reverse all signs
            end.y = -end.y;
            end.z = -end.z;
            end.w = -end.w;
        }
        
        // Calculate coefficients
        r32 sclp, sclq;
        
        if((1.0f - cosom) > 0.0001f) // 0.0001 -> some epsillon
        {
            // Standard case (slerp)
            r32 omega, sinom;
            omega = acos(cosom); // extract theta from dot product's cos theta
            sinom = sin(omega);
            sclp = sin((1.0f - f) * omega) / sinom;
            sclq = sin(f * omega) / sinom;
        } 
        else
        {
            // Very close, do linear interp (because it's faster)
            sclp = 1.0f - f;
            sclq = f;
        }
        
        Quat out;
        
        out.x = sclp * q0.x + sclq * end.x;
        out.y = sclp * q0.y + sclq * end.y;
        out.z = sclp * q0.z + sclq * end.z;
        out.w = sclp * q0.w + sclq * end.w;
        return out;
    }
    
    inline Mat4 transpose(Mat4 in)
    {
        Mat4 result(in);
        result.m11 = in.m11;
        result.m12 = in.m21;
        result.m13 = in.m31;
        result.m14 = in.m41;
        result.m21 = in.m12;
        result.m22 = in.m22;
        result.m23 = in.m32;
        result.m24 = in.m42;
        result.m31 = in.m13;
        result.m32 = in.m23;
        result.m33 = in.m33;
        result.m34 = in.m43;
        result.m41 = in.m14;
        result.m42 = in.m24;
        result.m43 = in.m34;
        result.m44 = in.m44;
        return result;
    }
    
    inline Mat4 to_matrix(Quat q)
    {
        Mat4 result(1.0f);
        
        result[0][0] = 1.0f - 2.0f * q.y * q.y - 2.0f * q.z * q.z;
        result[0][1] = 2.0f * q.x * q.y + 2.0f * q.z * q.w;
        result[0][2] = 2.0f * q.x * q.z - 2.0f * q.y * q.w;
        result[1][0] = 2.0f * q.x * q.y - 2.0f * q.z * q.w;
        result[1][1] = 1.0f - 2.0f * q.x * q.x - 2.0f * q.z * q.z;
        result[1][2] = 2.0f * q.y * q.z + 2.0f * q.x * q.w;
        result[2][0] = 2.0f * q.x * q.z + 2.0f * q.y * q.w;
        result[2][1] = 2.0f * q.y * q.z - 2.0f * q.x * q.w;
        result[2][2] = 1.0f - 2.0f * q.x * q.x - 2.0f * q.y * q.y;
        
        return result;
    }
    
    inline Vec4 transform(Mat4& m, const Vec4& v)
    {
        Vec4 r(0.0f);
        
        r.x = v.x * m[0][0] + v.y * m[0][1] + v.z * m[0][2] + v.w * m[0][3];
        r.y = v.x * m[1][0] + v.y * m[1][1] + v.z * m[1][2] + v.w * m[1][3];
        r.z = v.x * m[2][0] + v.y * m[2][1] + v.z * m[2][2] + v.w * m[2][3];
        r.w = v.x * m[3][0] + v.y * m[3][1] + v.z * m[3][2] + v.w * m[3][3];
        
        return r;
    }
    
    inline Vec3 operator*(Mat4 m, const Vec3& v)
    {
        Vec3 r = transform(m,Vec4(v,1.0f)).xyz;
        return r;
    }
    
    inline Vec4 operator*(Mat4 m, const Vec4& v)
    {
        Vec4 r = transform(m,v);
        return r;
    }
    
    void print_matrix(Mat4 In)
    {
        debug("%f %f %f %f\n", In[0][0],In[0][1],In[0][2],In[0][3]);
        debug("%f %f %f %f\n", In[1][0],In[1][1],In[1][2],In[1][3]);
        debug("%f %f %f %f\n", In[2][0],In[2][1],In[2][2],In[2][3]);
        debug("%f %f %f %f\n", In[3][0],In[3][1],In[3][2],In[3][3]);
    }
    
    void print_quat(Quat Q)
    {
        debug("(%f, %f, %f, %f)\n", Q.x, Q.y, Q.z, Q.w);
    }
    
    inline Vec4 operator*(const Vec4& v, const Mat4& m)
    {
        Vec4 result(0.0f);
        result.x = m.a * v.x + m.b * v.y + m.c * v.z + m.d * v.w;
        result.y = m.e * v.x + m.f * v.y + m.g * v.z + m.h * v.w;
        result.z = m.i * v.x + m.j * v.y + m.k * v.z + m.l * v.w;
        result.w = m.m * v.x + m.n * v.y + m.o * v.z + m.p * v.w;
        return result;
    }
    
    inline Vec3 operator*(const Vec3& v, const Mat4& m)
    {
        Vec3 result(0.0f);
        result.x = m.a * v.x + m.b * v.y + m.c * v.z + m.d * 1.0f;
        result.y = m.e * v.x + m.f * v.y + m.g * v.z + m.h * 1.0f;
        result.z = m.i * v.x + m.j * v.y + m.k * v.z + m.l * 1.0f;
        return result;
    }
    
    inline r32 determinant(const Mat4& in)
    {
        return in.m11 * in.m22 * in.m33 * in.m44 + in.m11 * in.m23 * in.m34 * in.m42 + in.m11 * in.m24 * in.m32 * in.m43 + 
            in.m12 * in.m21 * in.m34 * in.m43 + in.m12 * in.m23 * in.m31 * in.m44 + in.m12 * in.m24 * in.m33 * in.m41 +
            in.m13 * in.m21 * in.m32 * in.m44 + in.m13 * in.m22 * in.m34 * in.m41 + in.m13 * in.m24 * in.m31 * in.m42 +
            in.m14 * in.m21 * in.m33 * in.m42 + in.m14 * in.m22 * in.m31 * in.m43 + in.m14 * in.m23 * in.m32 * in.m41 -
            in.m11 * in.m22 * in.m34 * in.m43 - in.m11 * in.m23 * in.m32 * in.m44 - in.m11 * in.m24 * in.m33 * in.m42 -
            in.m12 * in.m21 * in.m33 * in.m44 - in.m12 * in.m23 * in.m34 * in.m41 - in.m12 * in.m24 * in.m31 * in.m43 -
            in.m13 * in.m21 * in.m34 * in.m42 - in.m13 * in.m22 * in.m31 * in.m44 - in.m13 * in.m24 * in.m32 * in.m41 -
            in.m14 * in.m21 * in.m32 * in.m43 - in.m14 * in.m22 * in.m33 * in.m41 - in.m14 * in.m23 * in.m31 * in.m42;
        
    }
    
    /*
    * I got this shit from stackoverflow and it took long enough to do
    * Has been tested by doing: Matrix * Inverse(Matrix) = I checks and seems to 
    * consistently work! 
    * Link: https://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
    */
    inline Mat4 inverse(Mat4 m)
    {
        Mat4 result(0.0f);
        
        auto e = m.q;
        
        result.q[0] = 
            e[5]  *   e[10] * e[15] - 
            e[5]  *   e[11] * e[14] -
            e[9]  *   e[6]  * e[15] +
            e[9]  *   e[7]  * e[14] +
            e[13] *   e[6]  * e[11] -
            e[13] *   e[7]  * e[10];
        
        // DONe
        
        result.q[4] = 
            -e[4] *   e[10] * e[15] +
            e[4]  *   e[11] * e[14] +
            e[8]  *   e[6]  * e[15] -
            e[8]  *   e[7]  * e[14] -
            e[12] *   e[6]  * e[11] +
            e[12] *   e[7]  * e[10];
        
        // DONE
        
        result.q[8] = 
            e[4]  *   e[9]  * e[15] - 
            e[4]  *   e[11] * e[13] -
            e[8]  *   e[5]  * e[15] +
            e[8]  *   e[7]  * e[13] +
            e[12] *   e[5]  * e[11] -
            e[12] *   e[7]  * e[9];
        //DONE
        
        result.q[12] = 
            -e[4] *   e[9]  * e[14] +
            e[4]  *   e[10] * e[13] +
            e[8]  *   e[5]  * e[14] -
            e[8]  *   e[6]  * e[13] -
            e[12] *   e[5]  * e[10] +
            e[12] *   e[6]  * e[9];
        //DONE
        
        result.q[1] = 
            -e[1] *   e[10] * e[15] +
            e[1]  *   e[11] * e[14] +
            e[9]  *   e[2]  * e[15] -
            e[9]  *   e[3]  * e[14] -
            e[13] *   e[2]  * e[11] +
            e[13] *   e[3]  * e[10];
        //DONE
        
        result.q[5] = 
            e[0]  *   e[10] * e[15] - 
            e[0]  *   e[11] * e[14] -
            e[8]  *   e[2]  * e[15] +
            e[8]  *   e[3]  * e[14] +
            e[12] *   e[2]  * e[11] -
            e[12] *   e[3]  * e[10];
        //DONE
        
        result.q[9] = 
            -e[0] *   e[9]  * e[15] + 
            e[0]  *   e[11] * e[13] +
            e[8]  *   e[1]  * e[15] -
            e[8]  *   e[3]  * e[13] -
            e[12] *   e[1]  * e[11] +
            e[12] *   e[3]  * e[9];
        //DONE
        
        result.q[13] = 
            e[0]  *   e[9]  * e[14] - 
            e[0]  *   e[10] * e[13] -
            e[8]  *   e[1]  * e[14] +
            e[8]  *   e[2]  * e[13] +
            e[12] *   e[1]  * e[10] -
            e[12] *   e[2]  * e[9];
        //DONE
        
        result.q[2] = 
            e[1]  *   e[6]  * e[15] - 
            e[1]  *   e[7]  * e[14] -
            e[5]  *   e[2]  * e[15] +
            e[5]  *   e[3]  * e[14] +
            e[13] *   e[2]  * e[7]  -
            e[13] *   e[3]  * e[6];
        //DONE
        
        result.q[6] = 
            -e[0] *   e[6]  * e[15] + 
            e[0]  *   e[7]  * e[14] +
            e[4]  *   e[2]  * e[15] -
            e[4]  *   e[3]  * e[14] -
            e[12] *   e[2]  * e[7]  +
            e[12] *   e[3]  * e[6];
        //DONE
        
        result.q[10] = 
            e[0]  *   e[5]  * e[15] - 
            e[0]  *   e[7]  * e[13] -
            e[4]  *   e[1]  * e[15] +
            e[4]  *   e[3]  * e[13] +
            e[12] *   e[1]  * e[7]  -
            e[12] *   e[3]  * e[5];
        //DONE
        
        result.q[14] = 
            -e[0] *   e[5]  * e[14] +
            e[0]  *   e[6]  * e[13] +
            e[4]  *   e[1]  * e[14] -
            e[4]  *   e[2]  * e[13] -
            e[12] *   e[1]  * e[6]  +
            e[12] *   e[2]  * e[5];
        //DONE
        
        result.q[3] = 
            -e[1]  *   e[6]  * e[11] + 
            e[1]   *   e[7]  * e[10] +
            e[5]   *   e[2]  * e[11] -
            e[5]   *   e[3]  * e[10] -
            e[9]   *   e[2]  * e[7]  +
            e[9]   *   e[3]  * e[6];
        //DONE
        
        result.q[7] = 
            e[0]  *   e[6]  * e[11] - 
            e[0]  *   e[7]  * e[10] -
            e[4]  *   e[2]  * e[11] +
            e[4]  *   e[3]  * e[10] +
            e[8]  *   e[2]  * e[7]  -
            e[8]  *   e[3]  * e[6];
        //DONE
        
        result.q[11] =  
            -e[0]  *   e[5]  * e[11] +
            e[0]   *   e[7]  * e[9]  +
            e[4]   *   e[1]  * e[11] -
            e[4]   *   e[3]  * e[9]  -
            e[8]   *   e[1]  * e[7]  +
            e[8]   *   e[3]  * e[5];
        //DONE
        
        result.q[15] = 
            e[0]  *   e[5]  * e[10] - 
            e[0]  *   e[6]  * e[9]  -
            e[4]  *   e[1]  * e[10] +
            e[4]  *   e[2]  * e[9]  +
            e[8]  *   e[1]  * e[6]  -
            e[8]  *   e[2]  * e[5];
        
        auto det = determinant(m);
        det = 1.0f / det;
        
        result = result * det;
        
        return result;
    }
    
    inline r32 dot(Vec2 v1, Vec2 v2)
    {
        return v1.x * v2.x + v1.y + v2.y;
    }
    
    inline r32 dot(Vec3 v1, Vec3 v2)
    {
        return v1.x * v2.x + v1.y + v2.y + v1.z * v2.z;
    }
    
    inline r32 dot(Vec4 v1, Vec4 v2)
    {
        return v1.x * v2.x + v1.y + v2.y + v1.z * v2.z + v1.w * v2.w;
    }
    
    inline i32 dot(Vec2i v1, Vec2i v2)
    {
        return v1.x * v2.x + v1.y + v2.y;
    }
    
    inline i32 dot(Vec3i v1, Vec3i v2)
    {
        return v1.x * v2.x + v1.y + v2.y + v1.z + v2.z;
    }
    
    inline r32 distance(Vec2 v1, Vec2 v2)
    {
        return sqrt(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2));
    }
    
    inline r32 distance(Vec3 v1, Vec3 v2)
    {
        return sqrt(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2) + pow(v1.z - v2.z, 2));
    }
    
    inline r32 distance(Vec4 v1, Vec4 v2)
    {
        return sqrt(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2) + pow(v1.z - v2.z, 2) + pow(v1.w - v2.w,2));
    }
    
    inline i32 distance(Vec2i v1, Vec2i v2)
    {
        return (i32)(sqrt(pow((r32)v1.x - (r32)v2.x, 2) + pow((r32)v1.y - (r32)v2.y, 2)));
    }
    
    inline i32 distance(Vec3i v1, Vec3i v2)
    {
        return (i32)sqrt(pow((r32)v1.x - v2.x, 2) + pow((r32)v1.y - v2.y, 2) + pow((r32)v1.z - v2.z, 2));
    }
    
    inline r32 distance(Vec3i v1, Vec3 v2)
    {
        return sqrt(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2) + pow(v1.z - v2.z, 2));
    }
    
    
    inline i32 floor(r32 v)
    {
        return (i32)std::floor(v);
    }
    
    
    inline Vec2 floor(Vec2 v)
    {
        Vec2 result(v);
        result.x = (r32)floor(v.x);
        result.y = (r32)floor(v.y);
        return result;
    }
    
    inline Vec3 floor(Vec3 v)
    {
        Vec3 result(v);
        result.x = (r32)floor(v.x);
        result.y = (r32)floor(v.y);
        result.z = (r32)floor(v.z);
        return result;
    }
    
    inline i32 ceil(r32 v)
    {
        return (i32)std::ceil(v);
    }
    
    inline Vec2 ceil(Vec2 v)
    {
        Vec2 result(v);
        result.x = (r32)ceil(v.x);
        result.y = (r32)ceil(v.y);
        return result;
    }
    
    inline Vec3 ceil(Vec3 v)
    {
        Vec3 result(v);
        result.x = (r32)ceil(v.x);
        result.y = (r32)ceil(v.y);
        result.z = (r32)ceil(v.z);
        return result;
    }
    
    
    inline i32 round(r32 v)
    {
        r32 half_ceil = ((r32)ceil(v))/2.0f;
        if(v >= half_ceil)
        {
            return ceil(v);
        }
        else 
        {
            return floor(v);
        }
    }
    
    r32 absolute(r32 v)
    {
        return ABS(v);
    }
    
    Vec2 absolute(Vec2 v)
    {
        return math::Vec2(ABS(v.x), ABS(v.y));
    }
    
    Vec3 absolute(Vec3 v)
    {
        return math::Vec3(ABS(v.x), ABS(v.y), ABS(v.z));
    }
    
    Vec4 absolute(Vec4 v)
    {
        return math::Vec4((r32)ABS(v.x), (r32)ABS(v.y), (r32)ABS(v.z), (r32)ABS(v.w));
    }
    
    inline r32 square(r32 v)
    {
        return v * v;
    }
    
    inline r32 sqrt(r32 v)
    {
        return (r32)std::sqrt(v);
    }
    
    inline r32 pow(r32 v, i32 e)
    {
        return (r32)std::pow(v, e);
    }
    
    inline r32 sin(r32 v)
    {
        return (r32)std::sin(v);
    }
    
    inline r32 cos(r32 v)
    {
        return (r32)std::cos(v);
    }
    
    inline r32 acos(r32 v)
    {
        return (r32)std::acos(v);
    }
    
    inline r32 length(Vec2 v)
    {
        return sqrt(pow(v.x,2) + pow(v.y,2));
    }
    
    inline r32 length(Vec3 v)
    {
        return sqrt(pow(v.x,2) + pow(v.y,2) + pow(v.z,2));
    }
    
    inline r32 length(Vec4 v)
    {
        return sqrt(pow(v.x,2) + pow(v.y,2) + pow(v.z,2) + pow(v.w,2));
    }
    
    inline r32 length(Quat q)
    {
        return sqrt(pow(q.x, 2) + pow(q.y, 2) + pow(q.z, 2) + pow(q.w, 2));
    }
    
    inline Vec2 normalize(Vec2 v)
    {
        Vec2 result(v);
        auto l = length(v);
        if(l == 0.0f)
        {
            return result;
        }
        result /= l;
        return result;
    }
    
    inline Vec3 normalize(Vec3 v)
    {
        Vec3 result(v);
        auto l = length(v);
        if(l == 0.0f)
        {
            return result;
        }
        result /= l;
        return result;
    }
    
    inline Vec4 normalize(Vec4 v)
    {
        Vec4 result(v);
        auto l = length(v);
        if(l == 0.0f)
        {
            return result;
        }
        result /= l;
        return result;
    }
    
    inline r32 get_angle_in_radians(Quat q)
    {
        return acos(q.w) * 2.0f;
    }
    
    inline Vec3 get_axis(Quat q)
    {
        r32 angle = get_angle_in_radians(q);
        Vec3 result;
        result.x = q.x / sin(angle / 2.0f);
        result.y = q.y / sin(angle / 2.0f); 
        result.z = q.z / sin(angle / 2.0f); 
        return result;
    }
    
    inline Mat4 scale(Mat4 in, Vec3 scale)
    {
        Mat4 result(in);
        result.m11 = scale.x * result.m11;
        result.m22 = scale.y * result.m22;
        result.m33 = scale.z * result.m33;
        
        return result;
    }
    
    inline Mat4 translate(Mat4 in, Vec3 translation)
    {
        Mat4 result(1.0f);
        
        result[0][3] += translation.x;
        result[1][3] += translation.y;
        result[2][3] += translation.z;
        return(result * in);
    }
    
    inline Mat4 x_rotate(r32 angle)
    {
        angle *= DEGREE_IN_RADIANS;
        
        r32 c = cos(angle);
        r32 s = sin(angle);
        
        Mat4 r(1,0, 0,0,
               0,c,-s,0,
               0,s, c,0,
               0,0, 0,1);
        
        return r;
    }
    
    inline Mat4 y_rotate(r32 angle)
    {
        angle *= DEGREE_IN_RADIANS;
        
        r32 c = cos(angle);
        r32 s = sin(angle);
        
        Mat4 r(c, 0,s,0,
               0, 1,0,0,
               -s,0,c,0,
               0, 0,0,1);
        
        return r;
    }
    
    inline Mat4 z_rotate(r32 angle)
    {
        angle *= DEGREE_IN_RADIANS;
        
        r32 c = cos(angle);
        r32 s = sin(angle);
        
        Mat4 r(c,-s,0,0,
               s,c,0,0,
               0,0,1,0,
               0,0,0,1);
        
        return r;
    }
    
    inline Mat4 create_rotation(r32 x_angle, r32 y_angle, r32 z_angle)
    {
        Mat4 result(1.0f);
        result = y_rotate(y_angle) * x_rotate(x_angle) * z_rotate(z_angle) * result;
        return result;
    }
    
    inline Quat rotate(Quat in, r32 a, Vec3 axis)
    {
        Quat result(in);
        auto q = math::Quat(axis.x, axis.y, axis.z, DEGREE_IN_RADIANS * a);
        result = in * q;
        result = normalize(result);
        return result;
    }
    
    // https://gamedev.stackexchange.com/a/50545
    inline Vec3 rotate(Vec3 in, Quat q)
    {
        math::Vec3 u(q.x, q.y, q.z);
        
        r32 s = q.w;
        
        auto result = 2.0f * dot(u, in) * u 
            + (s * s - dot(u,u)) * in 
            + 2.0f * s * cross(u, in);
        return result;
    }
    
    inline Mat4 rotate(Mat4 m, Quat r)
    {
        Mat4 result(1.0f);
        result = to_matrix(r) * m;
        return result;
    }
    
    inline Vec3 right(Mat4 m)
    {
        return normalize(math::Vec3(m[0][0],
                                    m[1][0],
                                    m[2][0]));
    }
    
    inline Vec3 up(Mat4 m)
    {
        return normalize(math::Vec3(m[0][1],
                                    m[1][1],
                                    m[2][1]));
    }
    
    inline Vec3 forward(Mat4 m)
    {
        return normalize(math::Vec3(m[0][2],
                                    m[1][2],
                                    m[2][2]));
    }
    
    inline Vec3 translation(Mat4 m)
    {
        return math::Vec3(m[0][3],
                          m[1][3],
                          m[2][3]);
    }
    
    inline Vec3 scale(Mat4 m)
    {
        math::Vec3 result;
        result.x = length(right(m));
        result.y = length(up(m));
        result.z = length(forward(m));
        return result;
    }
    
    inline Vec3 project(Vec3 in, Mat4 m, Mat4 p, Vec4 viewport)
    {
        Vec3 result(1.0f);
        auto tmp = Vec4(in, 1.0f);
        tmp = m * tmp;
        tmp = p * tmp;
        tmp /= tmp.w;
        
        tmp = tmp * 0.5f + 0.5f;
        tmp.x = tmp.x * viewport.z + viewport.x;
        tmp.y = tmp.y * viewport.w + viewport.y;
        
        return Vec3(tmp.x,tmp.y,tmp.z);
    }
    
    inline Vec3 cross(Vec3 a, Vec3 b)
    {
        Vec3 result;
        
        result.x = a.y*b.z - a.z*b.y;
        result.y = a.z*b.x - a.x*b.z;
        result.z = a.x*b.y - a.y*b.x;
        
        return result;
    }
    
    inline Mat4 ortho(r32 left, r32 right, r32 bottom, r32 top, r32 near, r32 far)
    {
        Mat4 result(1.0f);
        result.m11 = 2.0f/(right - left);
        result.m22 = 2.0f/(top - bottom);
        result.m33 = (-2.0f)/(far - near);
        result.m34 = -((far + near)/(far - near));
        result.m14 = -((right + left)/(right - left));
        result.m24 = -((top + bottom)/(top - bottom));
        result.m44 = 1.0f;
        
        return result;
    }
    
    inline Mat4 look_at(Vec3 forward, Vec3 eye)
    {
        Vec3 right = normalize(cross(Vec3(0, 1, 0), forward));
        Vec3 up = cross(forward, right);
        
        Mat4 result(right.x,    right.y,     right.z,    0,
                    up.x,       up.y,        up.z,       0,
                    -forward.x, -forward.y,  -forward.z, 0,
                    0,          0,           0,          1);
        
        auto translation = result * Vec4(-eye, 1.0f);
        result[0][3] = translation.x;
        result[1][3] = translation.y;
        result[2][3] = translation.z;
        
        return(result);
    }
    
    inline Mat4 look_at_with_target(Vec3 eye, Vec3 target)
    {
        Vec3 forward = normalize(target - eye);
        return look_at(forward, eye);
    }
    
    inline Mat4 perspective(r32 aspect, r32 focal_length, r32 near, r32 far)
    {
        r32 a = 1.0f;
        r32 b = aspect;
        r32 c = focal_length;
        
        r32 n = near;
        r32 f = far;
        
        r32 d = (n + f) / (n - f);
        r32 e = (2 * f * n) / (n - f);
        
        Mat4 result =
        {
            a * c,    0,  0,  0,
            0,  b * c,  0,  0,
            0,    0,  d,  e,
            0,    0, -1,  0
        };
        
        return(result);
    }
    
    inline Vec3 mult_point_matrix(Vec3 in, Mat4 m)
    {
        math::Vec3 result(0.0f);
        result.x = in.x * m[0][0] + in.y * m[0][1] + in.z * m[0][2] + m[0][3];
        result.y = in.x * m[1][0] + in.y * m[1][1] + in.z * m[1][2] + m[1][3];
        result.z = in.x * m[2][0] + in.y * m[2][1] + in.z * m[2][2] + m[2][3];
        r32 w = in.x * m[3][0] + in.y * m[3][1] + in.z * m[3][2] + m[3][3];
        
        if(w != 1)
        {
            result.x /= w;
            result.y /= w;
            result.z /= w;
        }
        return result;
    }
    
    inline Vec3 un_project(Vec3 in, Mat4 model, Mat4 projection, Vec4i viewport)
    {
        auto inv = inverse(projection * model);
        
        auto tmp = Vec4(in,1.0f);
        tmp.x = (tmp.x - viewport.x) / viewport.z;
        tmp.y = (tmp.y - viewport.y) / viewport.w;
        tmp = tmp * 2 - 1;
        
        auto obj = inv * tmp;
        obj /= obj.w;
        
        return Vec3(obj.x,obj.y,obj.z);
    }
    
    inline r32 lerp(r32 a, r32 t, r32 b)
    {
        r32 result = (1.0f - MIN(1.0f, t)) * a + t * b;
        return result;
    }
    
    inline Vec2 lerp(Vec2 a, r32 t, Vec2 b)
    {
        //Assert(t <= 1.0f);
        Vec2 result(0.0f);
        result.x = lerp(a.x,t,b.x);
        result.y = lerp(a.y,t,b.y);
        return result;
    }
    
    inline Vec3 lerp(Vec3 a, r32 t, Vec3 b)
    {
        //Assert(t <= 1.0f);
        Vec3 result(0.0f);
        result.x = lerp(a.x,t,b.x);
        result.y = lerp(a.y,t,b.y);
        result.z = lerp(a.z,t,b.z);
        return result;
    }
    
    inline Vec4 lerp(Vec4 a, r32 t, Vec4 b)
    {
        //Assert(t <= 1.0f);
        Vec4 result(0.0f);
        result.x = lerp(a.x,t,b.x);
        result.y = lerp(a.y,t,b.y);
        result.z = lerp(a.z,t,b.z);
        result.w = lerp(a.w,t,b.w);
        return result;
    }
    
    inline Vec2 rotate_by_angle(Vec2 in, r32 angle)
    {
        math::Vec2 result;
        result.x = in.x * cos(angle) - in.y * sin(angle);
        result.y = in.x * sin(angle) + in.y * cos(angle);
        return result;
    }
    
    inline r32 angle_from_direction(Vec2 in)
    {
        return (r32)atan2(in.x, in.y);
    }
    
    inline void seed_random(u32 seed)
    {
        srand(seed);
    }
    
    inline i32 random_int(i32 from, i32 to)
    {
        return rand() % to + from;
    }
    
    inline r32 random_float(r32 from, r32 to)
    {
        return (rand() / (float)RAND_MAX * to) + from;
    }
    
    struct Ray
    {
        Vec3 origin;
        Vec3 target;
        Vec3 ray;
    };
    
    struct BoundingBox
    {
        Vec3 min;
        Vec3 max;
    };
    
    inline b32 aabb_ray_intersection(Ray r, BoundingBox b)
    {
        math::Vec3 lb = b.min;
        math::Vec3 rt = b.max;
        // r.dir is unit direction vector of ray
        math::Vec3 dirfrac;
        dirfrac.x = 1.0f / r.ray.x;
        dirfrac.y = 1.0f / r.ray.y;
        dirfrac.z = 1.0f / r.ray.z;
        
        // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
        // r.org is origin of ray
        r32 t1 = (lb.x - r.origin.x) * dirfrac.x;
        r32 t2 = (rt.x - r.origin.x) * dirfrac.x;
        r32 t3 = (lb.y - r.origin.y) * dirfrac.y;
        r32 t4 = (rt.y - r.origin.y) * dirfrac.y;
        r32 t5 = (lb.z - r.origin.z) * dirfrac.z;
        r32 t6 = (rt.z - r.origin.z) * dirfrac.z;
        
        float tmin = MAX(MAX(MIN(t1, t2), MIN(t3, t4)), MIN(t5, t6));
        float tmax = MIN(MIN(MAX(t1, t2), MAX(t3, t4)), MAX(t5, t6));
        
        // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
        if (tmax < 0)
        {
            //t = tmax;
            return false;
        }
        
        // if tmin > tmax, ray doesn't intersect AABB
        if (tmin > tmax)
        {
            //t = tmax;
            return false;
        }
        
        //t = tmin;
        return true;
    }
    
    inline b32 new_aabb_ray_intersection(Ray ray, BoundingBox b)
    {
        auto ray_dir = ray.ray;
        auto ray_origin = ray.origin;
        
        auto temp = 0.0f;
        auto tx_min = (b.min.x - ray_origin.x) / ray_dir.x;
        auto tx_max = (b.max.x - ray_origin.x) / ray_dir.x;
        
        if(tx_max < tx_min)
        {
            temp = tx_max;
            tx_max = tx_min;
            tx_min = temp;
        }
        
        auto ty_min = (b.min.y - ray_origin.y) / ray_dir.y;
        auto ty_max = (b.max.y - ray_origin.y) / ray_dir.y;
        
        if(ty_max < ty_min)
        {
            temp = ty_max;
            ty_max = ty_min;
            ty_min = temp;
        }
        
        auto tz_min = (b.min.z - ray_origin.z) / ray_dir.z;
        auto tz_max = (b.max.z - ray_origin.z) / ray_dir.z;
        
        if(tz_max < tz_min)
        {
            temp = tz_max;
            tz_max = tz_min;
            tz_min = temp;
        }
        
        auto t_min = (tx_min > ty_min) ? tx_min : ty_min;
        auto t_max = (tx_max < ty_max) ? tx_max : ty_max;
        
        if(tx_min > ty_max || ty_min > tx_max) return false;
        if(t_min > tz_max || tz_min > t_max) return false;
        if(tz_min > t_min) t_min = tz_min;
        if(tz_max < t_max) t_max = tz_max;
        
        return true;
    }
    
    inline Ray raycast_from_mouse_coordinates(r32 mouse_x, r32 mouse_y, Mat4 p, Mat4 v, r32 width, r32 height)
    {
        Ray ray;
        r32 x = 1.0f - (2.0f * mouse_x) / width;
        r32 y = (2.0f * mouse_y / height) - 1.0f;
        r32 z = 1.0f;
        Vec3 ray_nds = Vec3(x, y, z);
        Vec4 ray_clip = Vec4(ray_nds.xy, 1.0, 1.0f);
        Vec4 ray_eye = inverse(p) * ray_clip;
        ray_eye = Vec4(ray_eye.xy, 1.0f, 0.0f);
        Vec3 ray_world = (v * ray_eye).xyz;
        ray_world = normalize(ray_world);
        
        //printf("Ray world %f %f %f\n", ray_world.x, ray_world.y, ray_world.z);
        ray.ray = ray_world;
        return(ray);
    }
    
    inline Ray cast_picking_ray(r32 mouse_x, r32 mouse_y, Mat4 p, Mat4 v, r32 width, r32 height)
    {
        auto mx = 1.0f - (2.0f * mouse_x) / width;
        auto my = 1.0f - (2.0f * mouse_y / height);
        
        // 1.0f is the far plane in NDC
        auto mouse = inverse(p) * math::Vec3(mx, my, 1.0f);
        mouse.z = 1.0f;
        mouse = inverse(v) * mouse;
        
        // -1.0f is the near plane in NDC
        auto origin = inverse(p) * math::Vec3(mx, my, -1.0f);
        origin.z = 1.0f;
        origin = inverse(v) * origin;
        
        auto temp_ray = math::Vec4(mouse - origin, 0.0f);
        temp_ray = normalize(temp_ray);
        Ray ray;
        ray.origin = origin;
        ray.target = mouse;
        ray.ray = temp_ray.xyz;
        //printf("%f %f %f\n", ray.origin.x, ray.origin.y, ray.origin.z);
        
        ray.ray = raycast_from_mouse_coordinates(mouse_x, mouse_y, p, v, width, height).ray;
        return(ray);
    }
    
    inline Ray cast_ray(Vec3 origin, Vec3 target)
    {
        Ray ray;
        ray.origin = origin;
        ray.target = target;
        ray.ray = normalize(math::Vec4(target - origin, 0.0f)).xyz;
        return ray;
    }
    
    using Rgb = Vec3;
    using Rgba = Vec4;
    
#define COLOR_RED math::Rgba(1, 0, 0, 1)
#define COLOR_GREEN math::Rgba(0, 1, 0, 1)
#define COLOR_BLUE math::Rgba(0, 0, 1, 1)
#define COLOR_BLACK math::Rgba(0, 0, 0, 1)
#define COLOR_WHITE math::Rgba(1, 1, 1, 1)
}

#endif

struct Rect
{
    union
    {
        struct
        {
            r32 x;
            r32 y;
        };
        math::Vec2 position;
    };
    union
    {
        struct
        {
            r32 width;
            r32 height;
        };
        math::Vec2 size;
    };
    
    
    Rect() {}
    Rect(r32 x, r32 y, r32 width, r32 height) : x(x), y(y), width(width), height(height) {}
    Rect(i32 x, i32 y, i32 width, i32 height) : x((r32)x), y((r32)y), width((r32)width), height((r32)height) {}
};

struct Recti
{
    union
    {
        struct
        {
            i32 x;
            i32 y;
        };
        math::Vec2i position;
    };
    union
    {
        struct
        {
            i32 width;
            i32 height;
        };
        math::Vec2i size;
    };
    
    
    Recti() {}
    Recti(i32 x, i32 y, i32 width, i32 height) : x(x), y(y), width(width), height(height) {}
};

inline b32 point_inside_rect(math::Vec2i point, Recti rect)
{
    return point.x >= rect.x && rect.y >= rect.y && point.x < rect.x + rect.width && point.y < rect.y + rect.height;
}

inline r32 sign(math::Vec2 p1, math::Vec2 p2, math::Vec2 p3)
{
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

inline math::Vec2 to_cartesian(math::Vec2 position)
{
    // @Cleanup: Move these to a global variable or similar
    r32 tile_width_half = 0.5f;
    r32 tile_height_half = 0.25f;
    
    math::Vec2 temp_pt;
    
    temp_pt.x = (position.x / tile_width_half + position.y / tile_height_half) / 2.0f;
    temp_pt.y = (position.y / tile_height_half - position.x / tile_width_half) / 2.0f;
    return temp_pt;
}

inline math::Vec2 to_isometric(math::Vec2 position)
{
    // @Cleanup: Move these to a global variable or similar
    r32 tile_width_half = 0.5f;
    r32 tile_height_half = 0.25f;
    
    math::Vec2 temp_pt;
    temp_pt.x = (position.x - position.y) * tile_width_half;
    temp_pt.y = (position.x + position.y) * tile_height_half;
    //return tempPt;
    return position;
}

inline b32 point_in_triangle(math::Vec2 pt, math::Vec2 v1, math::Vec2 v2, math::Vec2 v3)
{
    bool b1, b2, b3;
    
    b1 = sign(pt, v1, v2) < 0.0f;
    b2 = sign(pt, v2, v3) < 0.0f;
    b3 = sign(pt, v3, v1) < 0.0f;
    
    return ((b1 == b2) && (b2 == b3));
}

#endif
