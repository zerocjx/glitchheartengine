#version 150
in vec4 c;

out vec4 outColor;

void main()
{
    outColor = c;
}